FROM nginx:latest

USER root
WORKDIR /app

COPY index.html /app

EXPOSE 80

CMD ["nginx"]
